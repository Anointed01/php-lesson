<?php

$title = $content = $published_at = "";
$titleErr = $contentErr = $published_atErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    require 'includes/database.php';

    if (empty($_POST["title"])) {
        $titleErr = "Title is required";
      } else {
        $title = test_input($_POST["title"]);
      }
    if (empty($_POST["content"])) {
        $contentErr = "Content is required";
      } else {
        $content = test_input($_POST["content"]);
      }
    if (empty($_POST["published_at"])) {
        $published_atErr = "Time and Date is required";
      } else {
        $published_at = test_input($_POST["published_at"]);
    }

    if(!empty($title) || !empty($content) || !empty($published_at)) {

    $sql = "INSERT INTO articles (title, content, published_at)
            VALUES (?, ?, ?)";

    $stmt = mysqli_prepare($conn, $sql);

    if ($stmt === false) {

        echo mysqli_error($conn);

    } else {

        mysqli_stmt_bind_param($stmt, "sss", $_POST['title'], $_POST['content'], $_POST['published_at']);

        if (mysqli_stmt_execute($stmt)) {

            $id = mysqli_insert_id($conn);
            echo "Inserted record with ID: $id";

        } else {

            echo mysqli_stmt_error($stmt);

        }
    }
}
else {
    echo "Please fill in the following fields";
}   
}


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

?>
<?php require 'includes/header.php'; ?>

<h2>New article</h2>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

    <div>
        <label for="title">Title</label>
        <input name="title" id="title" placeholder="Article title">
        <span class="error">* <?php echo $titleErr;?></span>
    </div>

    <div>
        <label for="content">Content</label>
        <textarea name="content" rows="4" cols="40" id="content" placeholder="Article content"></textarea>
        <span class="error">* <?php echo $contentErr;?></span>

    </div>

    <div>
        <label for="published_at">Publication date and time</label>
        <input type="datetime-local" name="published_at" id="published_at">
        <span class="error">* <?php echo $published_atErr;?></span>
    </div>

    <button>Add</button>

</form>

<?php require 'includes/footer.php'; ?>
